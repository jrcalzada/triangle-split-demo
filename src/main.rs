#[derive(Debug, Clone, Copy, PartialEq)]
struct Point {
    x: f64,
    y: f64,
    z: f64,
}

impl Point {
    fn midpoint(&self, other: &Point) -> Point {
        Point {
            x: (self.x + other.x) / 2.0,
            y: (self.y + other.y) / 2.0,
            z: (self.z + other.z) / 2.0,
        }
    }
}

#[derive(Debug, Clone)]
struct Triangle {
    a: Point,
    b: Point,
    c: Point,
}

impl Triangle {
    fn new(a: Point, b: Point, c: Point) -> Triangle {
        Triangle { a, b, c }
    }

    fn split(&self, midpoint: Point) -> [Triangle; 3] {
        [
            Triangle::new(self.a, self.b, midpoint),
            Triangle::new(self.b, self.c, midpoint),
            Triangle::new(self.c, self.a, midpoint),
        ]
    }
}


fn center_of_edge(tri1: &Triangle, tri2: &Triangle) -> Option<Point> {
    let mut shared_points = Vec::new();
    let tri1_points = [tri1.a, tri1.b, tri1.c];
    let tri2_points = [tri2.a, tri2.b, tri2.c];

    for p1 in &tri1_points {
        for p2 in &tri2_points {
            if p1 == p2 {
                shared_points.push(*p1);
            }
        }
    }

    if shared_points.len() == 2 {
        Some(shared_points[0].midpoint(&shared_points[1]))
    } else {
        None
    }
}

fn split_triangles(tri1: &Triangle, tri2: &Triangle) -> Option<([Triangle; 3], [Triangle; 3])> {
    if let Some(midpoint) = center_of_edge(tri1, tri2) {
        Some((tri1.split(midpoint), tri2.split(midpoint)))
    } else {
        None
    }
}

type Node = (f64, f64);
type Element = (usize, usize, usize);


impl From<Point> for Node {
    fn from(point: Point) -> Self {
        (point.x, point.y)
    }
}


fn find_adjacent_elements(elements: &[Element]) -> Vec<(usize, usize)> {
    let mut adjacent_pairs = Vec::new();

    for (i, e1) in elements.iter().enumerate() {
        for (j, e2) in elements.iter().enumerate() {
            let shared_nodes = [e1.0, e1.1, e1.2]
                .iter()
                .filter(|&&node| node == e2.0 || node == e2.1 || node == e2.2)
                .count();
            if shared_nodes == 2 && i != j {
                adjacent_pairs.push((i, j));
            }
        }
    }

    adjacent_pairs
}


fn update_mesh(nodes: &mut Vec<Node>, elements: &mut Vec<Element>) {
    let adjacent_pairs = find_adjacent_elements(elements);

    for (i, j) in adjacent_pairs {
        let tri1 = Triangle::new(
            Point {
                x: nodes[elements[i].0].0,
                y: nodes[elements[i].0].1,
                z: 0.0,
            },
            Point {
                x: nodes[elements[i].1].0,
                y: nodes[elements[i].1].1,
                z: 0.0,
            },
            Point {
                x: nodes[elements[i].2].0,
                y: nodes[elements[i].2].1,
                z: 0.0,
            },
        );

        let tri2 = Triangle::new(
            Point {
                x: nodes[elements[j].0].0,
                y: nodes[elements[j].0].1,
                z: 0.0,
            },
            Point {
                x: nodes[elements[j].1].0,
                y: nodes[elements[j].1].1,
                z: 0.0,
            },
            Point {
                x: nodes[elements[j].2].0,
                y: nodes[elements[j].2].1,
                z: 0.0,
            },
        );

        if let Some((split_tri1, split_tri2)) = split_triangles(&tri1, &tri2) {
            let mut new_nodes = Vec::new();
            for t in split_tri1.iter().chain(split_tri2.iter()) {
                for p in &[t.a, t.b, t.c] {
                    if !nodes.contains(&(*p).into()) {
                        nodes.push((*p).into());
                        new_nodes.push(*p);
                    }
                }
            }

            let update_element = |tri: &Triangle| -> Element {
                (
                    nodes
                        .iter()
                        .position(|&n| n == (tri.a.x, tri.a.y))
                        .unwrap(),
                    nodes
                        .iter()
                        .position(|&n| n == (tri.b.x, tri.b.y))
                        .unwrap(),
                    nodes
                        .iter()
                        .position(|&n| n == (tri.c.x, tri.c.y))
                        .unwrap(),
                )
            };

            elements[i] = update_element(&split_tri1[0]);
            elements[j] = update_element(&split_tri1[1]);
            elements.push(update_element(&split_tri1[2]));
            elements.push(update_element(&split_tri2[0]));
            elements.push(update_element(&split_tri2[1]));
            elements.push(update_element(&split_tri2[2]));
        }
    }
}


fn main() {
    let mut nodes: Vec<Node> = vec![
        (0.0, 0.0),
        (1.0, 0.0),
        (0.0, 1.0),
        (1.0, 1.0),
    ];

    let mut elements: Vec<Element> = vec![
        (0, 1, 2),
        (1, 2, 3),
    ];

    println!("Original mesh:");
    println!("Nodes: {:?}", nodes);
    println!("Elements: {:?}", elements);

    update_mesh(&mut nodes, &mut elements);

    println!("Updated mesh:");
    println!("Nodes: {:?}", nodes);
    println!("Elements: {:?}", elements);
}


